#define _GNU_SOURCE

#include <ctype.h>
#include <netdb.h>
#include <openssl/bn.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include "roman.h"


enum{
    // Port numbers are in the range 1-65535, plus null byte
    PORT_SIZE = 8,
    // Largest number buffer needs to handle is 20 plus leading FDR char
    // plus trailing null byte
    MAX_BUFF_SIZE = 256,
    // Setting max size for fibonacci return
    FIB_SIZE = 128
};

const char *local_host = "::1";

int udp_server(const char *ip, char *port_num);

void end_program(int signal);

void upper_case(char *strings);

void fib_calc(int number);

char *fibonacci(char *num_string);

int num_check(char *string);

char *big_num(char * string);


int main(int argc, char *argv[])
{
    if(argc != 1){
        fprintf(stderr, "USAGE: %s\n", argv[0]);
        return 1;
    }

    // Setting up port numbers based on user Id, was adapted from code
    // supplied in /share/network/day02/udp_server.c
    char port_low[PORT_SIZE];
    char port_mid[PORT_SIZE];
    char port_high[PORT_SIZE];

    snprintf(port_low, sizeof(port_low), "%hu", getuid());
    snprintf(port_mid, sizeof(port_mid), "%hu", getuid() + 1000);
    snprintf(port_high, sizeof(port_high), "%hu", getuid() + 2000);

    // Forking the children for each port:
    // UID, UID + 1000, and UID 2000
    printf("Program listing on local address ::1\n");
    printf("Program listing on ports UID UID+1000 & UID+2000\n");
    // Set up for forked server taken from example code giveing in class
    // from /share/operating_systems/code/fork/fork-02.c
    // nested if-else staments for forking servers obatined with help of
    // A Dow
    // Allows for the ability to not setup any server if one fails
    pid_t server_low = fork();
    if(server_low == -1){
        fprintf(stderr, "Unable to fork Low Socket Server");
        return 2;
    }else if(server_low == 0){
        // Start server on ip ::1 on port $UID
        int error_low = udp_server(local_host, port_low);
        exit(error_low);
    }else{
        pid_t server_mid = fork();
        if(server_mid == -1){
            fprintf(stderr, "Unable to fork Low Socket Server");
            return 2;
        }else if(server_mid == 0){
            // Start server on ip ::1 on port $UID + 1000
            int error_mid = udp_server(local_host, port_mid);
            exit(error_mid);
        }else{
            pid_t server_high = fork();
            if(server_high == -1){
                fprintf(stderr, "Unable to fork Low Socket Server");
                return 2;
            }else if(server_high == 0){
                // Start server on ip ::1 on port $UID + 2000
                int error_high = udp_server(local_host, port_high);
                exit(error_high);
            }else{
                // Parent process just waits for children to finish
                waitpid(-1, NULL, 0);
            }
        }
    }
}

int udp_server(const char *ip, char *port_num)
{
    // Setting up sockets was adapted from code (next 4 lines)
    // supplied in /share/network/day02/udp_server.c
    struct addrinfo *results;
    struct addrinfo hints = {0};
    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;

    // Sigaction code taken from in class example
    // share/network/day03/resurse-search.c
    struct sigaction close_handler = {0};
    close_handler.sa_handler = end_program;
    sigaction(SIGTERM, &close_handler, NULL);
    sigaction(SIGINT, &close_handler, NULL);


    // Setting up sockets was adapted from code (next 50 lines)
    // supplied in /share/network/day02/udp_server.c
    int err = getaddrinfo(ip, port_num, &hints, &results);
    if(err != 0){
        fprintf(stderr, "Could not parse address: %s\n", gai_strerror(err));
        return 2;
    }

    int sd = socket(results->ai_family, results->ai_socktype, 0);
    if(sd < 0) {
        perror("Could not create socket");
        freeaddrinfo(results);
        return 3;
    }

    err = bind(sd, results->ai_addr, results->ai_addrlen);
    if(err < 0){
        perror("Could not create bind");
        close(sd);
        freeaddrinfo(results);
        return 4;
    }

    freeaddrinfo(results);

    for(;;) {
        char buf[MAX_BUFF_SIZE];
        struct sockaddr_storage client;
        socklen_t client_sz = sizeof(client);

        // Recieve the data
        ssize_t received = recvfrom(sd, buf, sizeof(buf), 0,
                (struct sockaddr *)&client, &client_sz);
        if(received < 0){
            perror("Problem receiving");
        }

        // NULL terminate the buffer and remove new line if present
        buf[received-1] = '\0';

        // Parse off the flag character and shift the data we want
        // down on the buffer
        int service = buf[0];
        for(int i = 1; i <= received; ++i){
            buf[i-1] = buf[i];
        }

        // set up strings for each function her, since c does not like
        // creating a new variable immediatily after a label
        char return_string[MAX_BUFF_SIZE];
        memset(return_string, 0, MAX_BUFF_SIZE);
        char *fib_return;
        int conversion = 0;

        // Switch case to determine which function to respond back with
        // Allowing both upper and lowercase
        switch(service){
        // Case Fibonacci Function
        case 70:
        case 102:
            fib_return = fibonacci(buf);
            // Always be checking, if return is negative an invalid
            // string was sent
            if(!fib_return){
                snprintf(return_string, MAX_BUFF_SIZE, "%s",
                         "Fibonacci string not within range 0-300.");
                break;
            }
            snprintf(return_string, MAX_BUFF_SIZE, "%s\n", fib_return);
            // If user sent Uppercase send it back as uppercase
            if(service == 70){
                upper_case(return_string);
            }
            // Our valgrind overlords decree that we free any malloced space
            free(fib_return);
            break;
        // Case Decimal to Hex converter
        case 68:
        case 100:;
            int error = num_check(buf);
            // Ensure string is all base 10 integers
            if(error < 0){
                snprintf(return_string, MAX_BUFF_SIZE, "%s",
                         "Decimal string must not contain characters");
                return -1;
            }
            // Convert input string into hex string
            char *hex_str = big_num(buf);
            // Checking return value to ensure conversion happened
            if(!hex_str){
                snprintf(return_string, MAX_BUFF_SIZE, "%s",
                         "Unable to convert BIGNUM to Hex");
            }
            snprintf(return_string, MAX_BUFF_SIZE, "0x%s\n", hex_str);
            // If user sent Uppercase send it back as uppercase
            if(service == 68){
                upper_case(return_string);
            }
            // Our valgrind overlords decree that we free any malloced space
            free(hex_str);
            break;
        // Case Roman Numeral to Hex Conversion
        case 82:
        case 114:
            // Function converts Roman unmerals to a single integer, to view
            // in hex output is set as such
            conversion = convert(buf);
            // Always be checking, if return is negative an invalid
            // string was sent
            if(conversion < 0){
                snprintf(return_string, MAX_BUFF_SIZE, "%s",
                         "Roman Numerals can only ha M,D,C,L,X,V, and/or"
                         " I and following proper protocol.\n");
                break;
            }
            snprintf(return_string, MAX_BUFF_SIZE, "0x%x\n", conversion);
            // If user sent Uppercase send it back as uppercase
            if(service == 82){
                upper_case(return_string);
            }
            break;
        // Default, first letter in recieved string was not F/D/R/f/d/r
        default:
            snprintf(return_string, MAX_BUFF_SIZE, "%s",
                     "Valid input string must start with"
                     " F,D,R,d,f, or r.\n");
        }

        // Send the packet
        ssize_t sent = sendto(sd, return_string, strlen(return_string), 0,
                               (struct sockaddr *)&client, client_sz);
        if(sent == 0){
            // Something went wrong, but this is the world or UDP
            // We don't worry about a packet not arriving at the
            // destinatioon
            printf("Enable to send UDP Packet\n");
        }
    }
    close(sd);
}

void end_program(int signal)
{
    // Recieved signal that the server is wanting to shutdown
    // Ensure graceful exit
    printf("Program ending, enacting port closing protocol.\n");
    signal = 0;
    exit(signal);
}


void upper_case(char *string)
{
    // Convert the hex string to uppercase
    for(size_t i = 2; i < strlen(string); ++i){
        string[i] = toupper(string[i]);
    }
}

char *fibonacci(char *num_string)
{
    char *error;
    // Converting input string to integer
    int fib_num = strtol(num_string, &error, 10);
    if(*error){
        return NULL;
    }
    // Input number must be between 0-300 inclusivly
    if(fib_num < 0 || fib_num > 300){
        return NULL;
    }
    // Setting up output variables for assembly function call
    unsigned long num1 = 0;
    unsigned long num2 = 0;
    unsigned long num3 = 0;
    unsigned long num4 = 0;
    fib_calc(fib_num);
    // Outputting the assembly results with inline assembly
    // done with the help of S Ciullo
    __asm__(
        "mov %0, r12\n"
        "mov %1, r13\n"
        "mov %2, r14\n"
        "mov %3, r15"
        : "=r"(num1), "=r"(num2), "=r"(num3), "=r"(num4)
    );
    char *result = calloc(1, FIB_SIZE);
    snprintf(result, FIB_SIZE, "0x%lx%lx%lx%lx", num1, num2, num3, num4);
    return result;
}

int num_check(char *string)
{
    // Small function to check  that the string passed only contains digits
    for(size_t i = 0; i < strlen(string); ++i){
        if(isdigit(string[i]) == 0){
            return -1;
        }
    }
    return 0;
}

char *big_num(char *string)
{
    // Use of BIGNUM for hex conversion obtained through J Haubric and
    // W Echlin

    // Creating the BIGNUM strcuture
    BIGNUM *num_str = BN_new();
    if(!num_str){
        return NULL;
    }
    // inserting integer string into the BIGNUM
    BN_dec2bn(&num_str, string);
    // Converting 
    char *hex_out = BN_bn2hex(num_str);
    if(!hex_out){
        BN_free(num_str);
        return NULL;
    }
    // Our valgrind overlords decree that we free any malloced space
    BN_free(num_str);
    return hex_out;
}
