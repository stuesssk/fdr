#include "roman.h"

int char_value(char numeral)
{
    // Converting the Roman Numeral to an integer value
    int value = 0;

    switch(numeral){
    case 'I':
    case 'i':
        value = 1;
        break;
    case 'V':
    case 'v':
        value = 5;
        break;
    case 'X':
    case 'x':
        value = 10;
        break;
    case 'L':
    case 'l':
        value = 50;
        break;
    case 'C':
    case 'c':
        value = 100;
        break;
    case 'D':
    case 'd':
        value = 500;
        break;
    case 'M':
    case 'm':
        value = 1000;
        break;
    case '\0':
        value = 0;
        break;
    default:
        value = -1;
    }
    return value;
}

int convert(char *string)
{
    int count_v = 0;
    int count_l = 0;
    int count_d = 0;
    int char_count = 1;
    char last = 'Z';
    // Checking to ensure all rule are followed for romman numerals
    // All rules obtained from www.blackwasp.co/uk/RomanToNumeral.aspx
    for(size_t i = 0; i < strlen(string); ++i){
        if(string[i] == 'V' || string[i] == 'v'){
            ++count_v;
        }else if(string[i] == 'L' || string[i] == 'l'){
            ++count_l;
        }else if(string[i] == 'D' || string[i] == 'd'){
            ++count_d;
        }
        // Only allowed on instance of I, V, or L each
        if(count_d > 1 || count_l > 1 || count_v > 1){
            return -1;
        }

        // Checking for duplicates in a row
        if(string[i] == last){
            ++char_count;
            // Can not have more than 4 of the same characters in a row
            if(char_count == 5){
                return -1;
            }
        }else{
            // Resetting values
            char_count = 1;
            last = string[i];
        }
    }

    // Set up array to make final testing of decresing left to right
    // values easier
    int int_string[BUFF_SIZE];
    int max_digit = 1000;

    // Following code for loop for converting Roman Numerals to Decimal
    // adopted from www.cquestions.com/2011/08/c-program-to-convert-roman-
    // number-to.html
    int total = 0;
    int j = 0;
    for(size_t i = 0; i < strlen(string); ++i){
        int val1 = char_value(string[i]);

        if(val1 > max_digit){
            return -1;
        }

        if(i + 1 < strlen(string)){
            int val2 = char_value(string[i +1]);
            // If val1 >= val2 no subraction is necessary
            if (val1 >= val2){
                int_string[j] = val1;
                ++j;
            }else{
                // The only roman numerals that can be subtracted from
                // Another Roman numeral are I,X,C. Once these are subtracted
                // No other roman numeral my be as large or larger than
                // The number subtracted.
                if((val1 != 1 && val1 != 10 && val1 != 100) ||
                    val2 > val1 * 10){
                    return -1;
                }else{
                    // Adding the result of the subtracted par to the array
                    int_string[j] = val2 - val1;
                    max_digit = val1 - 1;
                    ++i;
                    ++j;
                }
            }
        }else{
            // We are at the end of the string and only option is to add
            // the value to the array
            int_string[j] = val1;
            ++j;
        }
    }

    int_string[j] = 0;
    for(int i = 0; i < j; ++i){
        // In a proper Roman numeral from left to right the numbers 
        // will be equal to the previous or less than.
        // If the next number to be add is greater than the current numbers
        // The input string is an invalid Roman numeral
        if(int_string[i] < int_string[i + 1]){
            return -1;
        }
        total += int_string[i];
    }
    return total;
}
