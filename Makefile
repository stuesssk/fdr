CPPFLAGS+=-Wall -Wextra -Wpedantic
CPPFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal
CPPFLAGS+=-Waggregate-return -Winline

CFLAGS+=-std=c11

LDLIBS+=-lm
LDLIBS+=-lssl
LDLIBS+=-lcrypto

ASFLAGS += -W
CFLAGS += -O1 -masm=intel -fno-asynchronous-unwind-tables -std=c11

BIN=fdr
DEPS=fdr.o roman.o fibonacci.o

$(BIN) : $(DEPS)

.PHONY: debug profile clean

debug: CFLAGS+=-g
debug: $(BIN)

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: $(BIN)

clean:
	$(RM) $(BIN) $(DEPS)
