.intel_syntax noprefix

.balign 16
.globl fib_calc
fib_calc:
    push rbp
    xor rcx, rcx
    mov rcx, rdi

    # Zero all the registers we need
    # Using non-volatile registers to ensure volatile registers
    # are perserved when exiting function.
    xor rbx, rbx
    xor r10, r10
    xor r11, r11
    xor r12, r12
    xor r13, r13
    xor r14, r14
    xor r15, r15
    #intialize to one to start sequence F(1) = 1
    mov rax, 1


fib_loop:
    # if ecx is zero we have reached the sequence number the user input
    test ecx, ecx
    je Output

    # Calculating the next fibonacci number
    xadd r15, rax # Lowest bits in output
    adc r10, r14
    adc r11, r13
    adc rbx, r12  # Highest bits in output

    # swap registers to preserve previous number in calulation
    xchg r10, r14
    xchg r11, r13
    xchg rbx, r12

    # decrement the counter, break when it hits zero
    dec ecx
    jne fib_loop

Output:
    # values already in proper registers for output  for C-code to
    # concatinate these Strings properl
    xor eax, eax
    pop rbp
    ret
