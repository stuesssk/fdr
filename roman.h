#ifndef ROMAN_H
#define ROMAN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum{
    // Will ensure I have enough space for all number up to MMMM
    BUFF_SIZE = 48,
};

int char_value(char numeral);

int convert(char *string);
#endif
